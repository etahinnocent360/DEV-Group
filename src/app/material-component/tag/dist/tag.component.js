"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TagComponent = void 0;
var core_1 = require("@angular/core");
var keycodes_1 = require("@angular/cdk/keycodes");
var TagComponent = /** @class */ (function () {
    function TagComponent() {
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = true;
        this.separatorKeysCodes = [keycodes_1.ENTER, keycodes_1.COMMA];
        this.tags = [];
    }
    TagComponent.prototype.ngOnInit = function () {
    };
    TagComponent.prototype.add = function (event) {
        var input = event.input;
        var value = event.value;
        if ((value || '').trim()) {
            this.tags.push({ name: value.trim() });
        }
        if (input) {
            input.value = '';
        }
    };
    TagComponent.prototype.remove = function (tag) {
        var index = this.tags.indexOf(tag);
        if (index >= 0) {
            this.tags.splice(index, 1);
        }
    };
    TagComponent = __decorate([
        core_1.Component({
            selector: 'app-tag',
            templateUrl: './tag.component.html',
            styleUrls: ['./tag.component.scss']
        })
    ], TagComponent);
    return TagComponent;
}());
exports.TagComponent = TagComponent;
