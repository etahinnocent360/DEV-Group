import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  private email!:string;
  private password!:string;
  public hide!: true;
  constructor(public authService: AuthService, private router:Router) { }

  ngOnInit(){
  }


  submit_form(){
    let credentials = {
      email:this.email,
      password:this.password
    }
    // this.authService.login(credentials).subscribe((response:any)=>{
    //   if(response.token){
    //     localStorage.setItem('auth_token', response.token);
    //     this.router.navigate(['/admin']);
    //   }
      
    // })
  }

}
