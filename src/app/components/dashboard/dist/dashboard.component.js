"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DashboardComponent = void 0;
var core_1 = require("@angular/core");
var dialog_body_component_1 = require("src/app/dialog-body/dialog-body.component");
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(authService, dialog) {
        this.authService = authService;
        this.dialog = dialog;
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent.prototype.open_dialog = function (message) {
        var _this = this;
        var dialogRef = this.dialog.open(dialog_body_component_1.DialogBodyComponent, {
            data: {
                message: message
            },
            width: '550px',
            height: '200px'
        });
        dialogRef.afterClosed().subscribe(function (confirm) {
            if (confirm) {
                _this.sign_out();
            }
        });
    };
    DashboardComponent.prototype.sign_out = function () {
        this.authService.logout();
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./dashboard.component.scss']
        })
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
