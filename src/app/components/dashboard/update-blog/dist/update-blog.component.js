"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.UpdateBlogComponent = void 0;
var core_1 = require("@angular/core");
var alert_dialog_body_component_1 = require("src/app/alert-dialog-body/alert-dialog-body.component");
var dialog_body_component_1 = require("src/app/dialog-body/dialog-body.component");
var UpdateBlogComponent = /** @class */ (function () {
    // blog_service: any;
    function UpdateBlogComponent(active_route, dialog, detail, image_service) {
        this.active_route = active_route;
        this.dialog = dialog;
        this.detail = detail;
        this.image_service = image_service;
        this.show_spinner = false;
        this.blog_props = {
            title: "",
            content: "",
            tags: [],
            feature_image: ""
        };
    }
    UpdateBlogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.active_route.params.subscribe(function (response) {
            _this.blog_id = response.id;
            _this.get_blog_info();
        });
    };
    UpdateBlogComponent.prototype.processFile = function (imageInput) {
        this.selectedFile = imageInput.files[0];
        this.previewImageLoad();
    };
    UpdateBlogComponent.prototype.previewImageLoad = function () {
        var _this = this;
        var reader = new FileReader();
        reader.onloadend = function (e) {
            _this.blog_props.feature_image = reader.result;
        };
        reader.readAsDataURL(this.selectedFile);
    };
    UpdateBlogComponent.prototype.open_dialog = function (message) {
        var _this = this;
        var dialogRef = this.dialog.open(dialog_body_component_1.DialogBodyComponent, {
            width: '550px',
            height: '200px',
            data: {
                message: message
            }
        });
        dialogRef.afterClosed().subscribe(function (confirm) {
            if (confirm) {
                _this.submit_blog();
            }
        });
    };
    UpdateBlogComponent.prototype.open_alert_dialog = function (message) {
        var dialogRef = this.dialog.open(alert_dialog_body_component_1.AlertDialogBodyComponent, {
            width: '550px',
            height: '200px',
            data: {
                message: message
            }
        });
    };
    UpdateBlogComponent.prototype.get_blog_info = function () {
        var _this = this;
        this.detail.get_single_blog(this.blog_id).subscribe(function (response) {
            _this.blog_props.title = response.single_blog.title;
            _this.blog_props.content = response.single_blog.content;
            _this.blog_props.feature_image = response.single_blog.feature_image;
            response.single_blog.tags.forEach(function (element) {
                _this.blog_props.tags.push(element);
            });
        });
    };
    UpdateBlogComponent.prototype.submit_blog = function () {
        return __awaiter(this, void 0, void 0, function () {
            var image_link, image_data, blog;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.show_spinner = true;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.image_service.upload_image(this.selectedFile).toPromise()];
                    case 1:
                        image_data = _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        image_link = this.blog_props.feature_image;
                        _a.label = 3;
                    case 3:
                        blog = {
                            title: this.blog_props.title,
                            content: this.blog_props.content,
                            feature_image: image_link
                        };
                        this.detail.update_blog(blog, this.blog_id).subscribe(function (response) {
                            _this.blog_id = response.blog_id;
                            _this.show_spinner = false;
                            _this.open_alert_dialog("Blog with the id: " + _this.blog_id + " has been updated");
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    UpdateBlogComponent = __decorate([
        core_1.Component({
            selector: 'app-update-blog',
            templateUrl: './update-blog.component.html',
            styleUrls: ['./update-blog.component.scss']
        })
    ], UpdateBlogComponent);
    return UpdateBlogComponent;
}());
exports.UpdateBlogComponent = UpdateBlogComponent;
