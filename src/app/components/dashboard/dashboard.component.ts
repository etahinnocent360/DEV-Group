import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  [x: string]: any;


  constructor(public authService: AuthService, public dialog: MatDialog) { }

  ngOnInit() {
  }

  open_dialog(message: string) {
    const dialogRef = this.dialog.open(DialogBodyComponent, {
      data: {
        message
      },
      width: '550px',
      height: '200px'
    });
    dialogRef.afterClosed().subscribe((confirm: boolean) => {
      if (confirm) {
        this.sign_out();
      }
    })
  }

  sign_out() {
    this.authService.logout();
  }
}