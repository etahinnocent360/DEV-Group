"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AddBlogComponent = void 0;
var core_1 = require("@angular/core");
var alert_dialog_body_component_1 = require("src/app/alert-dialog-body/alert-dialog-body.component");
var dialog_body_component_1 = require("src/app/dialog-body/dialog-body.component");
var tag_component_1 = require("src/app/material-component/tag/tag.component");
var AddBlogComponent = /** @class */ (function () {
    function AddBlogComponent(image_service, detail, dialog) {
        this.image_service = image_service;
        this.detail = detail;
        this.dialog = dialog;
        this.show_spinner = false;
    }
    AddBlogComponent.prototype.ngOnInit = function () {
    };
    AddBlogComponent.prototype.ngAfterViewInit = function () {
        this.tags = this.childReference.tags;
    };
    AddBlogComponent.prototype.processFile = function (imageInput) {
        this.selectedFile = imageInput.files[0];
        this.previewImageLoad();
    };
    AddBlogComponent.prototype.previewImageLoad = function () {
        var _this = this;
        var reader = new FileReader();
        reader.onloadend = function (e) {
            _this.preview_image = reader.result;
        };
        reader.readAsDataURL(this.selectedFile);
    };
    AddBlogComponent.prototype.open_dialog = function (message) {
        var _this = this;
        var dialogRef = this.dialog.open(dialog_body_component_1.DialogBodyComponent, {
            width: '550px',
            height: '200px',
            data: {
                message: message
            }
        });
        dialogRef.afterClosed().subscribe(function (confirm) {
            if (confirm) {
                _this.submit_blog();
            }
        });
    };
    AddBlogComponent.prototype.open_alert_dialog = function (message) {
        var dialogRef = this.dialog.open(alert_dialog_body_component_1.AlertDialogBodyComponent, {
            width: '550px',
            height: '200px',
            data: {
                message: message
            }
        });
    };
    AddBlogComponent.prototype.submit_blog = function () {
        var _this = this;
        this.show_spinner = true;
        this.image_service.upload_image(this.selectedFile).toPromise();
        var blog = {
            title: this.title,
            content: this.content,
            feature_image: this.image_data.link,
            tags: []
        };
        this.tags.map(function (element) {
            blog.tags.push(element["name"]);
        });
        this.detail.add_blog(blog).subscribe(function (response) {
            _this.blog_id = response.id;
            _this.show_spinner = false;
            _this.open_alert_dialog("Blog has been created with the id: " + _this.blog_id);
            _this.title = "";
            // this.content = "";
            _this.ckeditorContent = "";
            _this.preview_image = "";
            _this.tags = [];
        });
    };
    __decorate([
        core_1.ViewChild(tag_component_1.TagComponent, { static: false })
    ], AddBlogComponent.prototype, "childReference");
    AddBlogComponent = __decorate([
        core_1.Component({
            selector: 'app-add-blog',
            templateUrl: './add-blog.component.html',
            styleUrls: ['./add-blog.component.scss']
        })
    ], AddBlogComponent);
    return AddBlogComponent;
}());
exports.AddBlogComponent = AddBlogComponent;
