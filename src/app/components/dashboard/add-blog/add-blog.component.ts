
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AlertDialogBodyComponent } from 'src/app/alert-dialog-body/alert-dialog-body.component';
import { FeatureImageService } from 'src/app/api-calls/feature-image.service';
import { DetailsService } from 'src/app/details.service';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { TagComponent } from 'src/app/material-component/tag/tag.component';
@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.scss']
})
export class AddBlogComponent implements OnInit {
  public selectedFile!: File;
  public preview_image:any;
  public tags!: [];
  public title!: string;
  public content!: string;
  public blog_id!: string;
  public image_data!: string;
  public show_spinner: boolean = false;
  @ViewChild(TagComponent, {static:false}) childReference:any;
  ckeditorContent: any;
  constructor(public image_service: FeatureImageService, public detail:DetailsService, private dialog: MatDialog,) { }

  ngOnInit(){
  }
   ngAfterViewInit(){
      this.tags = this.childReference.tags;
    
  }
imageInput: any
  processFile(imageInput:any){
    this.selectedFile = imageInput.files[0];
    this.previewImageLoad();
  }
  previewImageLoad(){
    let reader = new FileReader();
    reader.onloadend = e =>{
      this.preview_image = reader.result;
    }
    reader.readAsDataURL(this.selectedFile);
  }

  open_dialog(message:string){
    let dialogRef = this.dialog.open(DialogBodyComponent, {
      width: '550px',
      height: '200px',
      data: {
        message
      }
      
    });
    dialogRef.afterClosed().subscribe((confirm:boolean)=>{
      if(confirm){
        this.submit_blog();
      }
    })
    
  }

  open_alert_dialog(message:string){
    const dialogRef = this.dialog.open(AlertDialogBodyComponent,{
      width:'550px',
      height: '200px',
      data:{
        message
      }
    });
  }

 submit_blog(){
      this.show_spinner = true;
      this.image_service.upload_image(this.selectedFile).toPromise();
      let blog = {
        title: this.title,
        content: this.content,
        feature_image:this.image_data.link,
        tags:[]
      }

      this.tags.map((element)=>{
        blog.tags.push(element["name"])
      });

      this.detail.add_blog(blog).subscribe((response:any)=>{
        this.blog_id = response.id;
        this.show_spinner = false;
        this.open_alert_dialog(`Blog has been created with the id: ${this.blog_id}`);
        this.title = "";
        // this.content = "";
        this.ckeditorContent= ""
        this.preview_image = "";
        this.tags = [];
      });

    }


}
