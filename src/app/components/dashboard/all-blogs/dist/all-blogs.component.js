"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AllBlogsComponent = void 0;
var core_1 = require("@angular/core");
var alert_dialog_body_component_1 = require("src/app/alert-dialog-body/alert-dialog-body.component");
var dialog_body_component_1 = require("src/app/dialog-body/dialog-body.component");
var AllBlogsComponent = /** @class */ (function () {
    function AllBlogsComponent(dialog, detail) {
        this.dialog = dialog;
        this.detail = detail;
        this.blogs = [];
        this.show_spinner = false;
    }
    AllBlogsComponent.prototype.ngOnInit = function () {
        this.load_all_blogs();
    };
    // load_all_blogs() {
    //   throw new Error('Method not implemented.');
    // }
    AllBlogsComponent.prototype.load_all_blogs = function () {
        var _this = this;
        this.detail.get_all_blogs().subscribe(function (response) {
            response.all_blogs.forEach(function (element) {
                _this.blogs.push(element);
            });
        });
    };
    AllBlogsComponent.prototype.open_dialog = function (message, blog_id) {
        var _this = this;
        var dialogRef = this.dialog.open(dialog_body_component_1.DialogBodyComponent, {
            data: {
                message: message
            },
            width: '550px',
            height: '200px'
        });
        dialogRef.afterClosed().subscribe(function (confirm) {
            if (confirm) {
                _this.delete_single_blog(blog_id);
            }
        });
    };
    AllBlogsComponent.prototype.open_alert_dialog = function (message) {
        var dialogRef = this.dialog.open(alert_dialog_body_component_1.AlertDialogBodyComponent, {
            width: '550px',
            height: '200px',
            data: {
                message: message
            }
        });
    };
    AllBlogsComponent.prototype.delete_single_blog = function (blog_id) {
        var _this = this;
        this.show_spinner = true;
        this.detail.delete_blog(blog_id).subscribe(function (response) {
            if (response) {
                _this.show_spinner = false;
                _this.open_alert_dialog("The blog was successfully deleted");
            }
        });
    };
    AllBlogsComponent = __decorate([
        core_1.Component({
            selector: 'app-all-blogs',
            templateUrl: './all-blogs.component.html',
            styleUrls: ['./all-blogs.component.scss']
        })
    ], AllBlogsComponent);
    return AllBlogsComponent;
}());
exports.AllBlogsComponent = AllBlogsComponent;
