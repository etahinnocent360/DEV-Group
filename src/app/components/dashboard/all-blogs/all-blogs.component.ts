import { Component, OnInit,OnChanges, ChangeDetectorRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AlertDialogBodyComponent } from 'src/app/alert-dialog-body/alert-dialog-body.component';
import { DetailsService } from 'src/app/details.service';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';


interface Blog{
  title:string,
  content:string,
  feature_image:string,
  tags: [],
  id:string
}

@Component({
  selector: 'app-all-blogs',
  templateUrl: './all-blogs.component.html',
  styleUrls: ['./all-blogs.component.scss']
})
export class AllBlogsComponent implements OnInit {
  public blogs: Array<Blog> = [];
  public deleted_blog_id!: string;
  public show_spinner:boolean = false;
  constructor(public dialog:MatDialog, public detail: DetailsService) { }

  ngOnInit() {
    this.load_all_blogs();
  }
  // load_all_blogs() {
  //   throw new Error('Method not implemented.');
  // }
  load_all_blogs(){
    this.detail.get_all_blogs().subscribe((response:any)=>{
      response.all_blogs.forEach((element:any) => {
        this.blogs.push(element);
      });
    })
  }

  open_dialog(message:string, blog_id:string): void {
    let dialogRef = this.dialog.open(DialogBodyComponent,{
      data: {
        message
      },
      width: '550px',
      height:'200px'
    })

    dialogRef.afterClosed().subscribe((confirm:boolean)=>{
      if(confirm){
        this.delete_single_blog(blog_id);
      }
    });
  }
    open_alert_dialog(message:string){
    let dialogRef = this.dialog.open(AlertDialogBodyComponent,{
      width:'550px',
      height: '200px',
      data:{
        message
      }
    });
  }



  delete_single_blog(blog_id:string){
    this.show_spinner = true;
    this.detail.delete_blog(blog_id).subscribe((response: any)=>{
      if(response){
        this.show_spinner = false;
        this.open_alert_dialog("The blog was successfully deleted");
      }
    })
  }

}
