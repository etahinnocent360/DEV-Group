"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppRoutingModule = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// Import all the components for which navigation service has to be activated 
var sign_in_component_1 = require("./components/sign-in/sign-in.component");
var sign_up_component_1 = require("./components/sign-up/sign-up.component");
var dashboard_component_1 = require("./components/dashboard/dashboard.component");
var forgot_password_component_1 = require("./components/forgot-password/forgot-password.component");
var verify_email_component_1 = require("./components/verify-email/verify-email.component");
var auth_guard_1 = require("./shared/guard/auth.guard");
var home_component_1 = require("./home/home.component");
var all_blogs_component_1 = require("./components/dashboard/all-blogs/all-blogs.component");
var update_blog_component_1 = require("./components/dashboard/update-blog/update-blog.component");
var add_blog_component_1 = require("./components/dashboard/add-blog/add-blog.component");
var routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'sign-in', component: sign_in_component_1.SignInComponent },
    { path: 'register-user', component: sign_up_component_1.SignUpComponent },
    {
        path: 'dashboard', component: dashboard_component_1.DashboardComponent, canActivate: [auth_guard_1.AuthGuard],
        children: [
            { path: 'add-blog', component: add_blog_component_1.AddBlogComponent },
            { path: 'all-blogs', component: all_blogs_component_1.AllBlogsComponent },
            { path: 'update-blog/:id', component: update_blog_component_1.UpdateBlogComponent },
        ]
    },
    { path: 'forgot-password', component: forgot_password_component_1.ForgotPasswordComponent },
    { path: 'verify-email-address', component: verify_email_component_1.VerifyEmailComponent },
    { path: 'home', component: home_component_1.HomeComponent },
    { path: '**', component: home_component_1.HomeComponent }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
