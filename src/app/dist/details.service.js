"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DetailsService = void 0;
var core_1 = require("@angular/core");
var DetailsService = /** @class */ (function () {
    function DetailsService(http) {
        this.http = http;
        this.add_blog_url = 'http://localhost:3000/blog';
        this.get_all_blogs_url = 'http://localhost:3000/all-blogs';
        this.get_single_blog_url = 'http://localhost:3000/blog/';
        this.delete_blog_url = 'http://localhost:3000/delete_blog/';
        this.update_blog_url = 'http://localhost:3000/update-blog/';
    }
    DetailsService.prototype.add_blog = function (blog_props) {
        return this.http.post(this.add_blog_url, blog_props);
    };
    DetailsService.prototype.get_all_blogs = function () {
        return this.http.get(this.get_all_blogs_url);
    };
    DetailsService.prototype.get_single_blog = function (blog_id) {
        return this.http.get(this.get_single_blog_url + blog_id);
    };
    DetailsService.prototype.update_blog = function (blog_props, blog_id) {
        return this.http.put(this.update_blog_url + blog_id, blog_props);
    };
    DetailsService.prototype.delete_blog = function (id) {
        return this.http["delete"](this.delete_blog_url + id);
    };
    DetailsService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], DetailsService);
    return DetailsService;
}());
exports.DetailsService = DetailsService;
