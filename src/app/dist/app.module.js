"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = void 0;
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
// Firebase services + enviorment module
var fire_1 = require("@angular/fire");
var auth_1 = require("@angular/fire/auth");
var firestore_1 = require("@angular/fire/firestore");
var environment_1 = require("../environments/environment");
var auth_service_1 = require("./shared/services/auth.service");
var dashboard_component_1 = require("./components/dashboard/dashboard.component");
var sign_in_component_1 = require("./components/sign-in/sign-in.component");
var sign_up_component_1 = require("./components/sign-up/sign-up.component");
var forgot_password_component_1 = require("./components/forgot-password/forgot-password.component");
var verify_email_component_1 = require("./components/verify-email/verify-email.component");
var home_component_1 = require("./home/home.component");
var update_blog_component_1 = require("./components/dashboard/update-blog/update-blog.component");
var all_blogs_component_1 = require("./components/dashboard/all-blogs/all-blogs.component");
var add_blog_component_1 = require("./components/dashboard/add-blog/add-blog.component");
var animations_1 = require("@angular/platform-browser/animations");
var alert_dialog_body_component_1 = require("./alert-dialog-body/alert-dialog-body.component");
var dialog_body_component_1 = require("./dialog-body/dialog-body.component");
// import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
var http_1 = require("@angular/common/http");
var forms_1 = require("@angular/forms");
var material_module_1 = require("./material/material.module");
var tag_component_1 = require("./material-component/tag/tag.component");
var details_service_1 = require("./details.service");
var blog_details_component_1 = require("./blog-details/blog-details.component");
var header_component_1 = require("./header/header.component");
var ng2_ckeditor_1 = require("ng2-ckeditor");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                dashboard_component_1.DashboardComponent,
                sign_in_component_1.SignInComponent,
                sign_up_component_1.SignUpComponent,
                forgot_password_component_1.ForgotPasswordComponent,
                verify_email_component_1.VerifyEmailComponent,
                home_component_1.HomeComponent,
                update_blog_component_1.UpdateBlogComponent,
                all_blogs_component_1.AllBlogsComponent,
                add_blog_component_1.AddBlogComponent,
                alert_dialog_body_component_1.AlertDialogBodyComponent,
                dialog_body_component_1.DialogBodyComponent,
                tag_component_1.TagComponent,
                blog_details_component_1.BlogDetailsComponent,
                header_component_1.HeaderComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                fire_1.AngularFireModule,
                // AngularFireModule.initializeApp(environment.firebase),
                auth_1.AngularFireAuthModule,
                firestore_1.AngularFirestoreModule,
                animations_1.BrowserAnimationsModule,
                // RichTextEditorAllModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                material_module_1.MaterialModule,
                ng2_ckeditor_1.CKEditorModule,
            ],
            providers: [auth_service_1.AuthService, details_service_1.DetailsService,
                { provide: fire_1.FirebaseOptionsToken, useValue: environment_1.environment.firebase }],
            bootstrap: [app_component_1.AppComponent],
            entryComponents: [dialog_body_component_1.DialogBodyComponent, alert_dialog_body_component_1.AlertDialogBodyComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
