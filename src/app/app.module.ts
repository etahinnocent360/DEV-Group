import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Firebase services + enviorment module
import { AngularFireModule, FirebaseOptionsToken } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

import { AuthService } from "./shared/services/auth.service";


import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './components/verify-email/verify-email.component';
import { HomeComponent } from './home/home.component';
import { UpdateBlogComponent } from './components/dashboard/update-blog/update-blog.component';
import { AllBlogsComponent } from './components/dashboard/all-blogs/all-blogs.component';
import { AddBlogComponent } from './components/dashboard/add-blog/add-blog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertDialogBodyComponent } from './alert-dialog-body/alert-dialog-body.component';
import { DialogBodyComponent } from './dialog-body/dialog-body.component';
// import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { TagComponent } from './material-component/tag/tag.component';
import { DetailsService } from './details.service';
import { BlogDetailsComponent } from './blog-details/blog-details.component';
import { HeaderComponent } from './header/header.component';
import { CKEditorModule } from 'ng2-ckeditor';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    HomeComponent,
    UpdateBlogComponent,
    AllBlogsComponent,
    AddBlogComponent,
    AlertDialogBodyComponent,
    DialogBodyComponent,
    TagComponent,
    BlogDetailsComponent,
    HeaderComponent
  ],
  imports:[
    BrowserModule,
    AppRoutingModule,
    AngularFireModule,
    // AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    BrowserAnimationsModule,
    // RichTextEditorAllModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CKEditorModule,
    
  ],

  providers: [AuthService,DetailsService,
    { provide: FirebaseOptionsToken, useValue: environment.firebase }],
  bootstrap: [AppComponent],
  entryComponents: [DialogBodyComponent,AlertDialogBodyComponent],
})
export class AppModule { }
