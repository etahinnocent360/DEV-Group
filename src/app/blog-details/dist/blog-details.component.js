"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BlogDetailsComponent = void 0;
var core_1 = require("@angular/core");
var BlogDetailsComponent = /** @class */ (function () {
    function BlogDetailsComponent(activate_route, detail) {
        this.activate_route = activate_route;
        this.detail = detail;
        this.blog_props = {
            title: "",
            content: "",
            feature_image: "",
            tags: [],
            created_at: ""
        };
    }
    BlogDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activate_route.params.subscribe(function (response) {
            _this.blog_id = response.id;
            _this.get_blog_details();
        });
    };
    BlogDetailsComponent.prototype.get_blog_details = function () {
        var _this = this;
        this.detail.get_single_blog(this.blog_id).subscribe(function (response) {
            _this.blog_props.title = response.single_blog.title;
            _this.blog_props.content = response.single_blog.content;
            _this.blog_props.feature_image = response.single_blog.feature_image;
            _this.blog_props.created_at = response.single_blog.created_at;
            response.single_blog.tags.foreach(function (element) {
                _this.blog_props.tags.push(element);
            });
        });
    };
    BlogDetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-blog-details',
            templateUrl: './blog-details.component.html',
            styleUrls: ['./blog-details.component.scss']
        })
    ], BlogDetailsComponent);
    return BlogDetailsComponent;
}());
exports.BlogDetailsComponent = BlogDetailsComponent;
