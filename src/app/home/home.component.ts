import { Component, OnInit } from '@angular/core';
import { DetailsService } from '../details.service';
interface Blog{
  title:string,
  feature_image:string,
  created_at:string,
  content:string
  id: string
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public all_blogs: Blog[] = [];

  constructor(public detail:DetailsService ) {}

  ngOnInit(){
    this.load_all_blogs();
  }
   load_all_blogs(){
    this.detail.get_all_blogs().subscribe((response:any)=>{
      response.all_blogs.forEach((element:any) => {
        this.all_blogs.push(element);
      });
    })
  }

}
