import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FeatureImageService {
 

  public imgur_url:string = 'https://api.imgur.com/3/image';
  public client_id:string = '7361adb43680526';
  constructor(public http:HttpClient) { }

  upload_image(_image_file:File){
    let formData = new FormData();
    formData.append('image',_image_file, _image_file.name);


    let headers = new HttpHeaders({
      "authorization": 'Client-ID '+this.client_id
    });

    return this.http.post(this.imgur_url , formData, {headers:headers});
  }
}
