"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.FeatureImageService = void 0;
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var FeatureImageService = /** @class */ (function () {
    function FeatureImageService(http) {
        this.http = http;
        this.imgur_url = 'https://api.imgur.com/3/image';
        this.client_id = '7361adb43680526';
    }
    FeatureImageService.prototype.upload_image = function (_image_file) {
        var formData = new FormData();
        formData.append('image', _image_file, _image_file.name);
        var headers = new http_1.HttpHeaders({
            "authorization": 'Client-ID ' + this.client_id
        });
        return this.http.post(this.imgur_url, formData, { headers: headers });
    };
    FeatureImageService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], FeatureImageService);
    return FeatureImageService;
}());
exports.FeatureImageService = FeatureImageService;
